'use strict';

/**
 * @ngdoc function
 * @name interTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the interTestApp
 */
angular.module('interTestApp')
  .controller('MainCtrl', function($scope, $modal) {

    $scope.handleButtonClick = function() {
      //create a new modal window
      $modal.open({
        templateUrl: '../views/modal-template.html',
        backdrop: true,
        windowClass: 'modal',
        controller: function($scope, $modalInstance) {
          //data for the step by step component
          $scope.data = [{
            id: 'userNameInput',
            tooltip: 'Please input your User name',
            title: 'Welcome!'
          }, {
            id: 'passwordInput',
            tooltip: 'And now your password',
          }, {
            id: 'submitButton',
            tooltip: 'Click Submit to send your values',
            placement: 'left'
          }];

          //handle user interaction
          $scope.submit = function() {
            $modalInstance.dismiss('submit');
          };
          $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
          };
        }
      });
    };
  });