'use strict';

/**
 * @ngdoc overview
 * @name interTestApp
 * @description
 * # interTestApp
 *
 * Main module of the application.
 */
var app = angular.module('interTestApp', ['ui.bootstrap']);

app.config(function($tooltipProvider) {
  $tooltipProvider.setTriggers({
    'show': 'focus'
  });
});