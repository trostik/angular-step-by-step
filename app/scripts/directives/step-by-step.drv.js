'use strict';

angular.module('interTestApp')
  .directive('stepByStep', function($compile) {
    return {
      restrict: 'A',
      scope: {
        stepByStepData: '='
      },
      link: function(scope, element) {
        element.parent().addClass('step-by-step');
        scope.index = 0;

        scope.attachNewAttributes = function(element, data){
          element.attr('popover', data.tooltip);
          if (data.placement) {
            element.attr('popover-placement', data.placement);
          }
          if (data.title) {
            element.attr('popover-title', data.title);
          }
          element.attr('popover-trigger', 'show');
        };

        scope.nextPopover = function(data) {
          var el = element.find('#' + data.id);
          var clone = el.clone();

          scope.attachNewAttributes(clone, data);

          //call only once (one) and then remove te listener
          clone.one('click', function() { 
            if (scope.index >= scope.stepByStepData.length) {
              return;
            }
            scope.nextPopover(scope.stepByStepData[scope.index++]);
          });

          el.replaceWith($compile(clone)(scope.$new())); //change old input with a new one

          //waiting for the next digest cycle
          setTimeout(function() {
            clone.trigger('show');
          }, 1);
        };

        scope.nextPopover(scope.stepByStepData[scope.index++]); //start
      }
    };
  });