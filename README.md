**REQUIREMENTS:**

* Create a step by step walkthrough component 
* When a user clicks a button a modal window is opened
* In the modal window there are text inputs and buttons
* The user should see a “guide” that tells him what to do implemented with tooltips/popovers
* Each time user clicks an item the next tooltip should be shown

**INPUT:**

data = [

     {id:<id>,tooltip:<text>,placement:<the position of the popover>},

     {id:<id>,tooltip:<text>,title: <the title of the popover>},

     {id:<id>,tooltip:<text>}

]

**title and position not mandatory*

**CONSTRAINTS:** 

* Make a component that can be easily reused.
* Make use of any tech you wish to use for this.

**OUTCOME:**

* Used AngularJS
* Used Yeoman for the project structure and boilerplate
* Created a directive called step-by-step.
* When adding it to an html element like <form ... step-by-step step-by-step-data="<INPUT DATA>"... > (doesn't have to be a form)

**PREVIEW:**

![1.png](https://bitbucket.org/repo/Ldngzp/images/1224750454-1.png)
![2.png](https://bitbucket.org/repo/Ldngzp/images/1712473428-2.png)